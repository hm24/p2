<?php
  session_start(); //start session
  session_unset(); // unset data
  session_destroy();

  header('location:index.php');
  exit();
?>