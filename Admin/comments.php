<?php
/*
===========================
  - Manage Comments page
  - You Edit | Delete | Approve Comments from here
===========================
*/
ob_start(); //output Buffering Start  //ob_gzhandler
session_start();
$pageTitle = 'Comments';



//===========Start Manage Page===================================================
function manage(){
        global $con;
        $stmt = $con->prepare("SELECT comments.*,items.Name,users.Username 
                FROM comments
                INNER JOIN items
                ON items.item_ID = comments.item_ID
                INNER JOIN users
                ON users.userId = comments.user_Id
                ORDER BY C_id DESC");
        $stmt->execute();
  
        $rows = $stmt->fetchAll();
        ?>
            <h1 class="text-center">Manage Comments</h1>
            <div class='container'>
            <?php if(!empty($rows)){ ?>
            <div class="table-responsive">
              <table class="main-table text-center table table-bordered">
               <tr>
                <td>#ID</td>
                <td>Comment</td>
                <td>Item Name</td>
                <td>User Name</td>
                <td>Add Date</td>
                <td>Control</td>
               </tr>
  
               <?php
               foreach($rows as $row){
                 echo "<tr>";
                    echo "<td>". $row['C_id'] . "</td>"; 
                    echo "<td>". $row['Comment'] ."</td>";
                    echo "<td>". $row['Name'] . "</td>";
                    echo "<td>". $row['Username'] . "</td>";
                    echo "<td>". $row['Comment_Date'] . "</td>";
                    echo "<td>
                         <a href='comments.php?do=Edit&C_id=".$row['C_id']. "'class='btn btn-success'><i class='fa fa-edit'></i> Edit</a>
                         <a href='comments.php?do=Delete&C_id=".$row['C_id']. "' class='btn btn-danger confirm'><i class='fa fa-close'></i> Delete</a>";
                         if($row['Status'] == 0){
                          echo "<a href='comments.php?do=Approve&C_id=".$row['C_id']. "' class='btn btn-info activate'><i class='fa fa-check'></i> Approve</a>";
                         }
                        echo "</td>"; 
                 echo "</tr>";
               }
               ?>
               
              </table>
            </div>
            <?php } else{
              echo "<div class='empty-rec'>There Is No Comments To Show</div>";
            } ?>
            </div>
               
              <?php
  }
  //===========End Manage Page===================================================
  
  //==============================================================
  // start Edit page
  
    function Edit($C_id){
      global $con;
      $stmt = $con ->prepare("SELECT * FROM comments WHERE C_id = ?");
                     $stmt->execute(array($C_id));
                     // fetch data
                     $row = $stmt->fetch();
                     // the row count
                     $count = $stmt -> rowCount();
                     //if there is id show form  
                     if($count > 0){?>   
    <h1 class="text-center">Edit Comment</h1>
      <div class="container">
         <form class="form-horizontal" action="?do=Update" method="POST">
         <input type="hidden" name="C_id" value='<?php echo $C_id ?>'/>
          <!-- start Comment field-->
           <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Comment</label>
             <div class="col-sm-10 col-md-4">
              <textarea class="form-control" name="comment"><?php echo $row['Comment']?></textarea>
             </div>
          </div>
          <!-- end Comment field-->
          
           <!-- start submit field-->
           <div class="form-group">
             <div class="col-sm-offset-2 col-sm-10">
              <input type="submit" value="Save" class="btn btn-primary btn-lg"/>
             </div>
          </div>
          <!-- end submit field-->
          
         </form>
      </div>
  <?php
      } // end if
      else {
       echo "<div class='container'>";
       $theMsg= "<div class='alert alert-dager'>no such id</div>"; 
       redirectHome($theMsg);
       echo "</div>";
     }
  }
  // end Edit page
  //==========================================================================

  
  
  // start  Update Page
  function Update(){
    echo "<div class='container'>";
            $C_id = $_POST['C_id'];
            $comment = $_POST['comment'];
              
              global $con;
        $stmt = $con ->prepare("UPDATE comments SET Comment = ?
                                WHERE C_id = ?");
                    $stmt->execute(array($comment,$C_id));
                    echo "<div class='container'>";
                    $theMsg= "<div class='alert alert-success'>".$stmt->rowCount() . ' record updated</div>';
                    redirectHome($theMsg,'back');
                    echo "</div>";
            //update DB with these data=====================
      echo "</div>";
   }//end Update
    //===============================================================================
  
    //=========Start delete Page====================================================
    function delete(){
      echo "<h1 class='text-center'>Delete Comment</h1> <div class='container'>";
                      $C_id= isset($_GET['C_id']) && is_numeric($_GET['C_id']) ? intval($_GET['C_id']) : 0 ;
                     $check = checkItem("C_id","comments",$C_id);
                     //if there is id show form   
                     if($check > 0){ 
                       global $con;
                      $stmt = $con ->prepare("DELETE FROM comments WHERE C_id = :zid ");
                      $stmt ->bindParam(':zid',$C_id);
                      $stmt ->execute();
                      $theMsg= "<div class='alert alert-success'>".$stmt->rowCount() . ' record Deleted</div>';
                      redirectHome($theMsg,'back');
                     }
                     else { $theMsg= "<div class='alert alert-danger'>This is ID not Exist</div>";
                      redirectHome($theMsg);
                    } 
                     echo "</div>";
    }
    //===========End delete Page===========================================
  
     //===========Start Activate Page===========================================
  function approve(){
    echo "<h1 class='text-center'>Approve Member</h1> <div class='container'>";
    $C_id= isset($_GET['C_id']) && is_numeric($_GET['C_id']) ? intval($_GET['C_id']) : 0 ;
    $check = checkItem("C_id","comments",$C_id);
    if($check > 0){ 
     global $con;
     $stmt = $con ->prepare("UPDATE comments SET Status = 1 WHERE C_id = ? LIMIT 1");
     $stmt->execute(array($C_id));
     //echo "<div class='container'>";
    $theMsg= "<div class='alert alert-success'>".$stmt->rowCount() . ' User Approved</div>';
     redirectHome($theMsg,'back');
     //echo "</div>";
    }  else {
      $theMsg= "<div class='alert alert-danger'>This is ID not Exist</div>";
      redirectHome($theMsg);
    } 
    echo "</div>";
  }
  //===========End Activate Page===========================================
  if (isset($_SESSION['username'])){
      include 'init.php';
     
      $do = isset($_GET['do']) ? $_GET['do'] : 'Manage';
      switch($do){
        //======Manage===========================================
        case 'Manage': 
                     manage();
              break;// end manage
        case 'Add': echo 'welcome to add page';
                     Add();
        break;// end Add
        case 'insert': echo 'welcome to insert page';
  
               if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                 insert();  
              } else{
                echo "<div class='container'>";
                $theMsg = '<div class="alert alert-danger">Sorry You can\'t Browse This Page Directly</div>';
                redirectHome($theMsg);
                echo "</div>";
              };
              break;// end insert
        case 'Edit': 
                     $C_id= isset($_GET['C_id']) && is_numeric($_GET['C_id']) ? intval($_GET['C_id']) : 0 ;
                     Edit($C_id);
                    break;// end Edit
        case 'Update': 
                      echo "<h1 class='text-center'>Update Comment</h1>";
                      if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                        Update();
          
                        } else{
                          echo "<div class='container'>";
                          $theMsg = '<div class="alert alert-danger">Sorry You can\'t Browse This Page Directly</div>';
                                redirectHome($theMsg);
                                echo "</div>";
                        }
        break;// end update
        case 'Delete': 
                      delete();
                    break;// end Delete
        case 'Approve':// start Activate
                       approve();
                       break;// end Activate
        default : echo 'welcome to Default page';
    };
  
      include $tpl.'footer.php';
  } else {
      header('location:index.php');
    exit();
  }
ob_end_flush();
?>