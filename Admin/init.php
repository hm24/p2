<?php
include 'connect.php';

//routes
$langdir   =  "include/language/";
$tpl       =  "include/templets/";
$func      =  "include/functions/"; // functions directory
$css       =  "layout/css/";
$js        =  "layout/js/";


//include important files

include $func . "functions.php";
include $langdir ."english.php";
include $tpl . 'header.php';

//include navbar on all pages except one with nonavabaar 
if (!isset($noupperbar)){ include $tpl.'upperbar.html'; }
if (!isset($nonavbar)){ include $tpl.'navbar.php'; }
?>